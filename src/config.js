const config = {
    MAX_ATTACHMENT_SIZE: 5000000,
    s3: {
      REGION: "us-east-1",
      BUCKET: "notes-app-upload-stack-serverless",
    },
    apiGateway: {
      REGION: "us-east-1",
      URL: "https://dkvc9mp301.execute-api.us-east-1.amazonaws.com/prod",
    },
    cognito: {
      REGION: "us-east-1",
      USER_POOL_ID: "us-east-1_lZbhGWyNY",
      APP_CLIENT_ID: "700nlk0ppstt9lj3vubhiguj4h",
      IDENTITY_POOL_ID: "us-east-1:9fe081fd-1e26-43cd-ba45-5dceafb5ffc6",
    },
  };
  
  export default config;